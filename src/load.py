#!/usr/bin/env python
# vim: textwidth=0 wrapmargin=0 tabstop=4 shiftwidth=4 softtabstop=4 smartindent smarttab
"""Plugin that tests that modules we bundle for plugins are present and working."""

from asyncio import events
import json
import threading
import time
import logging
import os
from threading import Lock, Thread
from typing import TYPE_CHECKING, Any, Callable, Deque, Dict, List, Mapping, NamedTuple, Optional
from typing import OrderedDict as OrderedDictT
from typing import Sequence, Union, cast
from collections import OrderedDict, defaultdict, deque
from dataclasses import dataclass

import killswitch
import timeout_session
import myNotebook as nb
import plug
from config import applongname, appname, appversion, config, debug_senders

# For compatibility with pre-5.0.0
if not hasattr(config, 'get_int'):
    config.get_int = config.getint

if not hasattr(config, 'get_str'):
    config.get_str = config.get

if not hasattr(config, 'get_bool'):
    config.get_bool = lambda key: bool(config.getint(key))

if not hasattr(config, 'get_list'):
    config.get_list = config.get

if TYPE_CHECKING:
    def _(x: str) -> str:
        return x


# This could also be returned from plugin_start3()
plugin_name = os.path.basename(os.path.dirname(__file__))

# Logger per found plugin, so the folder name is included in
# the logging format.
logger = logging.getLogger(f'{appname}.{plugin_name}')
if not logger.hasHandlers():
    level = logging.INFO  # So logger.info(...) is equivalent to print()

    logger.setLevel(level)
    logger_channel = logging.StreamHandler()
    logger_channel.setLevel(level)
    logger_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s:%(lineno)d:%(funcName)s: %(message)s')  # noqa: E501
    logger_formatter.default_time_format = '%Y-%m-%d %H:%M:%S'
    logger_formatter.default_msec_format = '%s.%03d'
    logger_channel.setFormatter(logger_formatter)
    logger.addHandler(logger_channel)


EVENT_DATA = Union[Mapping[str, Any], Sequence[Mapping[str, Any]]]


class Credentials(NamedTuple):
    """Credentials holds the set of credentials required to identify an EDCS payload."""

    cmdr: Optional[str]
    api_key: str
    fid: Optional[str]


@dataclass
class Event:
    """Event represents an event for the EDCS API."""

    name: str
    timestamp: str
    data: EVENT_DATA
    url: str
    method: str


class This:
    def __init__(self):
        self.session = timeout_session.new_session()
        self.base_url="https://edcs-bot-prod-clihq.ondigitalocean.app/api"
        self.thread: Thread
        self.events: Dict[str, Deque[Event]] = defaultdict(deque)
        self.event_lock: Lock = threading.Lock()  # protects events, for use when rewriting
        self.cmdr: Optional[str] = None
        self.FID: Optional[str] = None  # Frontier IDevents
        self.session.headers['Content-Type'] = 'application/json'


    def stats_url(self) -> str:
        return f'{self.base_url}/carrier/stats'


    def jump_url(self) -> str:
        return f'{self.base_url}/carrier/jump'


    def cancel_jump_url(self, id) -> str:
        return f'{self.base_url}/carrier/{id}/jump'

    def materials_url(self) -> str:
        return f'{self.base_url}/carrier/materials'


    def filter_events(self, key: str, predicate: Callable[[Event], bool]) -> None:
        """
        filter_events is the equivalent of running filter() on any event list in the events dict.

        it will automatically handle locking, and replacing the event list with the filtered version.

        :param key: the key to filter
        :param predicate: the predicate to use while filtering
        """
        with self.event_lock:
            tmp = self.events[key].copy()
            self.events[key].clear()
            self.events[key].extend(filter(predicate, tmp))


this = This()

_TIMEOUT = 20
WORKER_WAIT_TIME = 5  # Minimum time for worker to wait between sends

def plugin_start3(plugin_dir: str) -> str:
    logger.debug('Starting worker thread...')
    this.thread = Thread(target=new_worker, name='EDCS worker')
    this.thread.daemon = True
    this.thread.start()
    logger.debug('Done.')
    return plugin_name


def plugin_stop() -> None:
    """
    Plugin stop method.

    :return:
    """
    logger.info('Stopping')


def journal_entry(cmdr: str, is_beta: bool, system: str, station: str, entry: dict, state: dict) -> None:
    """
    Handle the given journal entry.

    :param cmdrname:
    :param is_beta:
    :param system:
    :param station:
    :param entry:
    :param state:
    :return: None
    """
    if is_beta:
        return

    should_return, new_entry = killswitch.check_killswitch('plugins.edcs.journal', entry, logger)
    if should_return:
        plug.show_error(_('EDCS disabled. See Log.'))  # LANG: INARA support disabled via killswitch
        logger.trace('returning due to killswitch match')
        return

    should_return, new_entry = killswitch.check_killswitch(
        f'plugins.edcs.journal.event.{entry["event"]}', new_entry, logger
    )
    if should_return:
        logger.trace('returning due to killswitch match')
        # this can and WILL break state, but if we're concerned about it sending bad data, we'd disable globally anyway
        return

    entry = new_entry
    event_name: str = entry.get('event')
    this.cmdr = cmdr
    this.FID = state['FID']

    logger.debug(
            f'cmdr = "{cmdr}", is_beta = "{is_beta}"'
            f', system = "{system}", station = "{station}"'
            f', event = "{entry["event"]}"'
    )
    if entry["event"] == "CarrierStats":
        to_send = {
            'FuelLevel': entry.get('FuelLevel'),
            'Name': entry.get('Name'),
            'CarrierId': entry.get('CarrierID'),
            'Callsign': entry.get('Callsign'),
            'DockingAccess': entry.get('DockingAccess'),
            'AllowNotorious': entry.get('AllowNotorious'),
            'Crew': entry.get('Crew'),
            'Finance': entry.get('Finance'),
            'ShipPacks': entry.get('ShipPacks'),
            'ModulePacks': entry.get('ModulePacks')
        }
        logger.info(f'Sending CarrierStats event: {to_send}')
        new_add_event(event_name, entry.get('timestamp'), to_send, this.stats_url(), cmdr, this.FID)

    elif entry["event"] == "CarrierJumpRequest":
        to_send = {
            'CarrierId': entry.get('CarrierID'),
            'SystemName': entry.get('SystemName'),
            'Body': entry.get('Body')
        }
        logger.info(f'Sending CarrierJump event: {to_send}')
        new_add_event(event_name, entry.get('timestamp'), to_send, this.jump_url(), cmdr, this.FID)
    elif entry["event"] == "CarrierJumpCancelled":
        new_add_event(event_name, entry.get('timestamp'), {}, this.cancel_jump_url(entry["CarrierID"]), cmdr, this.FID, 'DELETE')
    elif entry["event"] == "FCMaterials":
        to_send = {
            'MarketId': entry.get('MarketID'),
            'Timestamp': entry.get('timestamp'),
            'Items': list(map(lambda i: {
                'Name': i['Name_Localised'],
                'Price': i['Price'],
                'Stock': i['Stock'],
                'Demand': i['Demand']
            }, entry.get('Items')))
        }
        new_add_event(event_name, entry.get('timestamp'), to_send, this.materials_url(), cmdr, this.FID)


def credentials(cmdr: Optional[str]) -> Optional[str]:
    """
    Get the credentials for the current commander.

    :param cmdr: Commander name to search for credentials
    :return: Credentials for the given commander or None
    """
    if not cmdr:
        return None

    cmdrs = config.get_list('edcs_cmdrs', default=[])
    if cmdr in cmdrs and config.get_list('edcs_apikeys'):
        return config.get_list('edcs_apikeys')[cmdrs.index(cmdr)]

    else:
        return None


def new_add_event(
    name: str,
    timestamp: str,
    data: EVENT_DATA,
    url: str,
    cmdr: Optional[str] = None,
    fid: Optional[str] = None,
    method: str = 'POST'
):
    """
    Add a journal event to the queue, to be sent to edcs at the next opportunity.

    If provided, use the given cmdr name over the current one

    :param name: name of the event
    :param timestamp: timestamp of the event
    :param data: payload for the event
    :param cmdr: the commander to send as, defaults to the current commander
    """
    if cmdr is None:
        cmdr = this.cmdr

    if fid is None:
        fid = this.FID

    # api_key = credentials(this.cmdr)
    # if api_key is None:
    #     logger.warning(f"cannot find an API key for cmdr {this.cmdr!r}")
    #     return

    # key = Credentials(str(cmdr), api_key)  # this fails type checking due to `this` weirdness, hence str()

    with this.event_lock:
        event = Event(name, timestamp, data, url, method)
        logger.debug(f'Adding event to key {cmdr}: {event}')
        this.events[cmdr].append(event)


def new_worker():
    """
    Handle sending events to the EDCS API.

    Will only ever send one message per WORKER_WAIT_TIME, regardless of status.
    """
    logger.debug('Starting worker thread')
    while True:
        events = get_events()
        if (res := killswitch.get_disabled("plugins.edcs.worker")).disabled:
            logger.warning(f"EDCS worker disabled via killswitch. ({res.reason})")
            continue

        for cmdr, event_list in events.items():
            event_list = clean_event_list(event_list)
            if not event_list:
                continue

            for e in event_list:
                logger.info(f'Sending {e.name} event for {cmdr} to {e.url} with data: {json.dumps(e.data, separators=(",", ":"))}')
                try_send_data(e.url, e.data, e.method)

        time.sleep(WORKER_WAIT_TIME)

    logger.debug('Done.')


def clean_event_list(event_list: List[Event]) -> List[Event]:
    """Check for killswitched events and remove or modify them as requested."""
    out = []

    for e in event_list:
        bad, new_event = killswitch.check_killswitch(f'plugins.edcs.worker.{e.name}', e.data, logger)
        if bad:
            continue

        e.data = new_event
        out.append(e)

    return out


def get_events(clear: bool = True) -> Dict[str, List[Event]]:
    """
    Fetch a frozen copy of all events from the current queue.

    :param clear: whether or not to clear the queues as we go, defaults to True
    :return: the frozen event list
    """
    out: Dict[str, List[Event]] = {}
    with this.event_lock:
        for key, events in this.events.items():
            out[key] = list(events)
            if clear:
                events.clear()

    return out


def try_send_data(url: str, data: Mapping[str, Any], method: str) -> None:
    """
    Attempt repeatedly to send the payload forward.

    :param url: target URL for the payload
    :param data: the payload
    """
    for i in range(3):
        logger.debug(f"sending data to API, attempt #{i}")
        try:
            if send_data(url, data, method):
                break

        except Exception as e:
            logger.debug('unable to send events', exc_info=e)
            return


def send_data(url: str, data: Mapping[str, Any], method: str) -> bool:  # noqa: CCR001
    """
    Write a set of events to the edcs API.

    :param url: the target URL to post to
    :param data: the data to POST
    :return: success state
    """
    # NB: As of 2022-01-25 Artie has stated the EDCS API does *not* support compression
    if method == 'POST':
        r = this.session.post(url, data=json.dumps(data, separators=(',', ':')), timeout=_TIMEOUT)
    elif method == 'DELETE':
        r = this.session.delete(url, timeout=_TIMEOUT)
    else:
        return False

    r.raise_for_status()

    return True  # regardless of errors above, we DID manage to send it, therefore inform our caller as such
